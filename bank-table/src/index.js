import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
// import App from './components/App';
//
// ReactDOM.render(<App/>, document.getElementById('root'));

import WelcomePage from './components/WelcomePage';
import {BrowserRouter} from 'react-router-dom';

ReactDOM.render(<BrowserRouter>
      <WelcomePage/>
</BrowserRouter>, document.getElementById('root'));
