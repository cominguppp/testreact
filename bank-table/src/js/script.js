"use strict";

$(document).ready(function() {

      $('.transactions-amount').css('color', function() {
            var text = $(this).text();
            return (text < 0 ? 'red' : 'black');
      });

      $('.fas .fa-chevron-down .chevron').click(function(e) {
            e.removeClass('fa-chevron-down').addClass('fa-chevron-up');
      })

});
