import React from 'react';

const AccountTitle = () => {
      return (
            <div className="account-entry-container account-title-container col-sm-12">
                  <div className="account-entry-item col-sm-1">
                        <span />
                  </div>
                  <div className="account-entry-item col-sm-2">
                        <span>
                              <span>
                                    <div className="col-sm-12">
                                          Eingang
                                    </div>
                                    <div className="col-sm-12">
                                          Ausgang
                                    </div>
                              </span>
                        </span>
                  </div>
                  <div className="account-entry-item col-sm-2">
                        <span>
                              Bezeichnung
                        </span>
                  </div>
                  <div className="account-entry-item col-sm-3">
                        <span>
                              Sender / Empänger
                              Betreff
                        </span>
                  </div>
                  <div className="account-entry-item col-sm-2">
                        <span>
                              Betrag
                        </span>
                  </div>
                  <div className="account-entry-item col-sm-2">
                        <span>
                              Bunchnungstag Wertellung
                        </span>
                  </div>
            </div>
      )
}

export default AccountTitle
