import React, {Component} from 'react';
import Account from './Account';

class Accounts extends Component {
      render() {
            let bankInfo = this.props.accountInfo
            let transactions = this.props.transactionsInfo
            let finalResult = []
            finalResult.push(
                  <Account
                        accountName={bankInfo[0].accountName}
                        iban={bankInfo[0].iban}
                        lastSuccessfulUpdate={bankInfo[0].lastSuccessfulUpdate}
                        balance={bankInfo[0].balance}
                        transactionsInfo={transactions}
                        id={bankInfo[0].id}
                  />
            )

            return (
                  <section>
                        {finalResult}
                  </section>
            );
    }
}

export default Accounts
