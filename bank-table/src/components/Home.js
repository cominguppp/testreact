import React, {Component} from 'react';

const Home = () => (
<div>
      <div class="video-background">
            <div class="video-foreground">
                  <iframe src="https://www.youtube.com/embed/XdgsAm-cjMw?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen="allowfullscreen"></iframe>
            </div>
      </div>

      <div id="vidtop-content">
            <div class="vid-info">
                  <h1>tis is the home page</h1>
                  <p>text text text</p>
            </div>
      </div>
</div>);

export default Home
