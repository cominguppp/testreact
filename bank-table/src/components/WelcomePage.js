import React, {Component} from 'react';
import {Link, NavLink, Switch, Route} from 'react-router-dom';

import Title from './Title';
import Main from './Main';

class WelcomePage extends Component {
      render() {
            return (
                  <div className='app'>
                        <Title />
                        <Main />
                  </div>
            )

      }
}

export default WelcomePage
