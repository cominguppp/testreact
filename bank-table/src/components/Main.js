import React, {Component} from 'react';
import {Switch, Route} from 'react-router-dom';

import App from './App';
import Home from './Home';

const Main = () =>(
      <Switch>
            <Route exact="exact" path='/' component={Home}></Route>
            <Route exact="exact" path='/app' component={App}></Route>
      </Switch>
);

export default Main
