import React from 'react';
import Moment from 'moment';

const Transactions = ({counterpartName, purpose, amount, bankBookingDate, valueDate}) => {
      let transactionsBookingDate = Moment(bankBookingDate).format('DD-MM-YYYY')
      let transactionsValueDate = Moment(valueDate).format('DD-MM-YYYY')
      let amountHTML = []
      if (amount < 0) {
            amountHTML.push(
                  <span className="transactions-amount-red">
                        {amount} €
                  </span>
            )
      }
      else {
            amountHTML.push(
                  <span className="transactions-amount">
                        {amount} €
                  </span>
            )
      }

      return (
            <div className="account-entry-container col-sm-12">
                  <div className="account-entry-item col-sm-1">
                        <span />
                  </div>
                  <div className="account-entry-item col-sm-2">
                        <span>
                              <span>
                                    <i className="fas fa-sign-out-alt icon-red"></i>

                              </span>
                        </span>
                  </div>
                  <div className="account-entry-item col-sm-2">
                        <span>

                        </span>
                  </div>
                  <div className="account-entry-item col-sm-3 text-left">
                        <span>
                              <span>
                                    <div className="col-sm-12">
                                          {counterpartName}
                                    </div>
                                    <div className="col-sm-12">
                                          {purpose}
                                    </div>
                              </span>
                        </span>
                  </div>
                  <div className="account-entry-item col-sm-2">
                        {amountHTML}
                  </div>
                  <div className="account-entry-item col-sm-2">
                        <span>
                              <span>
                                    <div className="col-sm-12">
                                          {transactionsBookingDate}
                                    </div>
                                    <div className="col-sm-12">
                                          {transactionsValueDate}
                                    </div>
                              </span>
                        </span>
                  </div>
                  <br />
                  <br />
            </div>

      )
}

export default Transactions
