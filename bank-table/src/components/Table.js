import React, {Component} from 'react';

import Bank from './Bank';
import {getState} from 'redux';

class Table extends Component {

      render () {
            // let list = this.props
            let list = this.props.bankData.getState();
            let finalResult = []
            for(var i in list) {
                  if (i === "banking") {
                        finalResult.push(
                              <Bank
                                    bankId={list[i].connections[0].bank.id}
                                    bankName={list[i].connections[0].bank.name}
                                    bic={list[i].connections[0].bank.bic}
                                    bankDetail={list[i]}>
                              </Bank>
                        )
                  }
            }

            if (finalResult === []){
                  finalResult = 'No bank data';
            }

            return (
                  <div className="bank-table col-sm-10">
                        {finalResult}
                  </div>
            )
      }
}

export default Table
