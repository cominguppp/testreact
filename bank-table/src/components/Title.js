import React from 'react';
import {NavLink} from 'react-router-dom';

import {Navbar, Nav, NavItem, NavDropdown, MenuItem} from 'react-bootstrap';

const Navigation = () => (
      <Navbar className="nav-bar">
            <Navbar.Header>
                  <Navbar.Brand>
                        <NavLink to='/'>Home</NavLink>
                  </Navbar.Brand>
                  <Navbar.Brand>
                        <NavLink to='/app'>App</NavLink>
                  </Navbar.Brand>
            </Navbar.Header>
      </Navbar>
);

const Title = () => {
      return (<div className="header col-sm-12">
            <Navigation/>
      </div>);
}

export default Title
