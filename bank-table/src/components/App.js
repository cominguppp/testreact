import React, {Component} from 'react';
import axios from 'axios';
import jsonData from '../data/data.json';
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import {getState} from 'redux';

// Import Components
import Title from './Title';
import Table from './Table';

import GetData from '../actions/GetData';

class App extends Component {
      constructor(props, context) {
            super(props, context);
      }

      componentWillMount() {}

      render() {
            let bankData = createStore(GetData)
            console.log(bankData.getState());

            return (
                  <div>
                        {/* <Title/> */}
                        <Provider>
                              <Table bankData={bankData}/>
                        </Provider>
                  </div>
            )
      }
}

export default App
