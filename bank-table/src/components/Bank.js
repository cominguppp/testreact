import React, {Component} from 'react';
import {Collapse} from 'react-bootstrap';

import Accounts from './Accounts';

class Bank extends Component {
      constructor(props, context){
            super(props, context);

            this.state ={
                  open: false,
                  chevronClass: 'fas fa-chevron-down chevron'
            }

            this.collapseBank = this.collapseBank.bind(this)
      }



      collapseBank(){
            //Change image
            if (this.state.chevronClass === 'fas fa-chevron-down chevron') {
                  this.setState({
                        chevronClass: 'fas fa-chevron-up chevron',
                  })
            }
            else {
                  this.setState({
                        chevronClass: 'fas fa-chevron-down chevron',
                  })
            }

            //Update state
            this.setState({
                  open: !this.state.open,
            })
      }

      render(){
            let bankName = this.props.bankName
            let bic = this.props.bic
            let bankDetail = this.props.bankDetail

            let shortenname = bankName.charAt(0)

            return (
                  <div className="col-sm-12">
                        {/* Bank Session */}
                        <div className="bank-entry-container col-sm-12">
                              <div className="bank-entry-item name-shorten col-sm-1">
                                    <span>
                                          {shortenname}
                                    </span>
                              </div>
                              <div className="bank-entry-item col-sm-4">
                                    <span>
                                          <div className="bank-name col-sm-12">
                                                {bankName}
                                          </div>
                                          <div className="bank-bic col-sm-12">
                                                BIC: {bic}
                                          </div>
                                    </span>
                              </div>
                              <div className="bank-entry-item saldo col-sm-3">
                                    <span>Saldo per</span>
                              </div>
                              <div className="bank-entry-item saldo-in-currency col-sm-3">
                                    <span>Saldo in €</span>
                              </div>
                              <div className="bank-entry-item collapse-button col-sm-1">
                                    <span>
                                          <a onClick={this.collapseBank}>
                                                <i className={this.state.chevronClass}></i>
                                          </a>
                                    </span>
                              </div>
                        </div>

                        {/* Account Session */}

                        {/* <Collapse in={this.state.open} className="bank-account-container col-sm-12">
                              <Accounts accountInfo={bankDetail.accounts} transactionsInfo={bankDetail.transactions}/>
                        </Collapse> */}
                        <Collapse in={this.state.open}  className="bank-account-container col-sm-12">
                              <div>
                                    <Accounts accountInfo={bankDetail.accounts} transactionsInfo={bankDetail.transactions}/>
                              </div>
                        </Collapse>
                  </div>
            )
      }

}

export default Bank
