import React, {Component} from 'react';
import {Pagination} from 'react-bootstrap';

import Transactions from './Transactions';

class TransactionList extends Component {
      constructor(props) {
            super(props);
            this.getNewTransactionList = this.getNewTransactionList.bind(this)
      }

      getNewTransactionList(pageIndex) {
            // Call api
            // axios.get('./link')
            // .then(response => {
            //       this.setState({data: response.data});
            // })
            // .catch(function(error) {
            //       console.log(error);
            // });
      }

      render(){
            let transactions = this.props
            let finalResult = []
            let tmpItems = []
            let tmpPaging = []
            // use with multiple account
            for (var i in transactions) {
                  // console.log(i, list[i], (typeof list[i] === "connections"));
                  for (var item in transactions[i]) {
                        tmpItems.push(transactions[i][item].items)
                        tmpPaging.push(transactions[i][item].paging)
                        break;
                  }
            }

            let active = tmpPaging[0].page;
            let items = [];
            for (let number = 1; number <= tmpPaging[0].pageCount; number++) {
                  items.push(
                    <Pagination.Item
                          active={number === active}
                          onClick={() => this.getNewTransactionList(number)}
                    >
                          {number}
                    </Pagination.Item>
                  );
            }

            for (item in tmpItems[0]) {
                  finalResult.push(
                        <Transactions
                              counterpartName={tmpItems[0][item].counterpartName}
                              purpose={tmpItems[0][item].purpose}
                              amount={tmpItems[0][item].amount}
                              bankBookingDate={tmpItems[0][item].bankBookingDate}
                              valueDate={tmpItems[0][item].valueDate}
                        />
                  )
            }
            return (
                  <section className="col-sm-12">

                        <div className="col-sm-12 text-center">
                              {finalResult}
                        </div>
                        <div className="col-sm-12 text-center">
                              <Pagination bsSize="small" className="pagination-dock">
                                    {items}
                              </Pagination>
                        </div>
                  </section>
            );
      }
}

export default TransactionList
