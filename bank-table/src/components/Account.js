import React, {Component} from 'react';
import Moment from 'moment';
import { Collapse } from 'react-bootstrap';

import AccountTitle from './AccountTitle';
import TransactionList from './TransactionList.js';

class Account extends Component {
      constructor(props, context){
            super(props, context);

            this.state ={
                  open: false,
                  chevronClass: 'fas fa-chevron-down chevron'
            }

            this.collapseAccount = this.collapseAccount.bind(this)
      }

      collapseAccount = () => {
            //Change image
            if (this.state.chevronClass === 'fas fa-chevron-down chevron') {
                  this.setState({
                        chevronClass: 'fas fa-chevron-up chevron',
                  })
            }
            else {
                  this.setState({
                        chevronClass: 'fas fa-chevron-down chevron',
                  })
            }

            //Update state
            this.setState({
                  open: !this.state.open,
            })
      }

      render() {
            // Get data
            let accountName = this.props.accountName
            let iban = this.props.iban
            let lastSuccessfulUpdate = this.props.lastSuccessfulUpdate
            let transactions = this.props.transactionsInfo

            // Modify
            let date = Moment(lastSuccessfulUpdate).format('DD-MM-YYYY')
            let time = Moment(lastSuccessfulUpdate).format('hh:mm ')
            return (
                  <div className="col-sm-12">
                        <div className="account-entry-container col-sm-12">
                              <div className="account-entry-item fav-icon col-sm-1">
                                    <span>
                                          <i className="far fa-star icon-star"></i>
                                    </span>
                              </div>
                              <div className="account-entry-item col-sm-6">
                                    <span>
                                          <div className="bank-name col-sm-12">
                                                {accountName}
                                          </div>
                                          <div className="bank-bic col-sm-12">
                                                IBAN: {iban}
                                          </div>
                                    </span>
                              </div>
                              <div className="account-entry-item col-sm-2">
                                    <span>
                                          <div className="last-account-update-date col-sm-12">
                                                {date}
                                          </div>
                                          <div className="last-account-update-date col-sm-12">
                                                {time} Uhr
                                          </div>
                                    </span>
                              </div>
                              <div className="account-entry-item col-sm-2"></div>
                              <div className="account-entry-item collapse-button col-sm-1">
                                    <span>
                                          <a onClick={this.collapseAccount}>
                                                <i className={this.state.chevronClass}></i>
                                          </a>
                                    </span>
                              </div>
                        </div>
                        <Collapse in={this.state.open}  className="bank-account-container col-sm-12">
                              <div>
                                    <AccountTitle />
                                    <TransactionList transactionList={transactions} />
                              </div>
                        </Collapse>

                  </div>
            )
      }
}

export default Account
